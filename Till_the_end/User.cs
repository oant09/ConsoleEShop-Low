﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
	public abstract class User : Role
	{
		public Dictionary<int, Order> Orders { get; private set; }

		public string UserName { get; set; }

		public User(string userName) : base()
		{
			UserName = userName;
			Orders = new Dictionary<int, Order>();
		}

		public int CreateOrder(string itemName)
		{
			Item orderItem = SearchItem(itemName);
			if (orderItem is null)
				return -1;
			Order newOrder = Shop.CreateOrder(orderItem, this);
			Orders.Add(newOrder.OrderNumber, newOrder);
			return newOrder.OrderNumber;
		}

		public void ConfirmOrder(int orderNumber)
		{
			ChangeStatus(orderNumber, OrderStatus.Received); 
		}

		public void Logout()
		{

		}

		public abstract string ChangeStatus(int orderNumber, OrderStatus status);
	}
}
