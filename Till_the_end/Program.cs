﻿using System;
using System.Collections.Generic;
using System.Linq;



namespace Shop
{
	static class View
	{
		public delegate void PrintStartPage();

		public static void PrintStartPageGuest()
		{
			Console.WriteLine("\nHere is available options for you:") ;
			Console.WriteLine("1. Overview all items in the shop");
			Console.WriteLine("2. Search item by name");
			Console.WriteLine("3. Don't have an account yet? Register now!");
			Console.WriteLine("4. Already registered? Please log in for purchasing items");
			Console.WriteLine("5. Type 'exit' to close the shop");
			Console.WriteLine("Type option you want to proceed\n");
			
		}

		public static void PrintStartPageUser()
		{
			Console.WriteLine("\n1. Overview all items in the shop");
			Console.WriteLine("2. Search item by name");
			Console.WriteLine("3. CreateOrder");
			Console.WriteLine("4. Confirm or cancel your order");
			Console.WriteLine("5. View your order history");
			Console.WriteLine("6. Change your personal information");
			Console.WriteLine("7. Log out");
			Console.WriteLine("Type option you want to proceed\n");
		}

		public static void RegisteredUserPanel(string line, RegisteredUser rUser)
		{
			Console.WriteLine($"Welcome back, {rUser.UserName}. \nHere is available options for you:");
			PrintStartPageUser();
			while (!line.Equals("exit"))
			{
				line = Console.ReadLine();
				Int32.TryParse(line, out int input);
				switch (input)
				{
					case 1:
						PanelOptions.PrintShop(Shop.itemList.Values.ToList());
						break;
					case 2:
						PanelOptions.SearchItem(rUser);
						break;
					case 3:
						PanelOptions.CreateOrder(rUser, PrintStartPageUser);
						PrintStartPageUser();
						break;
					case 4:
						PanelOptions.ChangeStatus(rUser);
						break;
					case 5:
						PanelOptions.ViewOrderHistory(rUser);
						break;
					case 6:
						PanelOptions.ChangeInfo(rUser);
						break;
					case 7:
						Console.WriteLine("logout");
						return;
					default:
						Console.WriteLine("No such option available. Please, press correct number");
						break;
				}
			}
		}

		public static void PrintStartPageAdmin()
		{
			Console.WriteLine("\n1. Overview all items in the shop");
			Console.WriteLine("2. Search item by name");
			Console.WriteLine("3. CreateOrder");
			Console.WriteLine("4. Change status of order");
			Console.WriteLine("5. View user info");
			Console.WriteLine("6. Add new item to shop");
			Console.WriteLine("7. Change item information");
			Console.WriteLine("8. Log out");
			Console.WriteLine("Type option you want to proceed\n");
		}

		public static void AdminUserPanel(string line, Administrator rUser)
		{
			Console.WriteLine($"Welcome back, {rUser.UserName}. \nHere is available options for you:");
			PrintStartPageAdmin();
			while (true)
			{
				line = Console.ReadLine();
				Int32.TryParse(line, out int input);
				switch (input)
				{
					case 1:
						PanelOptions.PrintShop(Shop.itemList.Values.ToList());
						break;
					case 2:
						PanelOptions.SearchItem(rUser);
						PrintStartPageAdmin();
						break;
					case 3:
						PanelOptions.CreateOrder(rUser, PrintStartPageUser);
						PrintStartPageAdmin();
						break;
					case 4:
						PanelOptions.ChangeStatusAdmin(rUser);
						break;
					case 5:
						PanelOptions.ViewChangeUserInfo(rUser);
						break;
					case 6:
						PanelOptions.AddNewItem(rUser);
						break;
					case 7:
						PanelOptions.ChangeItemInfo(rUser);
						break;
					case 8:
						Console.WriteLine("logout");
						GuestPanel(ref line, new Guest());
						break;
					default:
						Console.WriteLine("No such option available. Please, press correct number");
						break;
				}
			}
		}

		static public bool GuestPanel(ref string line, Guest guest)
		{
			while (! line.Equals("exit"))
			{
				View.PrintStartPageGuest();
				line = Console.ReadLine();
				switch (line)
				{
					case "1":
						PanelOptions.PrintShop(guest.OverviewItems());
						PrintStartPageGuest();
						break;
					case "2":
						PanelOptions.SearchItem(guest);
						PrintStartPageGuest();
						break;
					case "3":
						PanelOptions.CreateUser(PrintStartPageGuest);
						break;
					case "4":
						if (!PanelOptions.LogIn(guest))
							PrintStartPageGuest();
						break;
					case "exit":
						Console.WriteLine("The shop is closing...");
						break;
					default:
						Console.WriteLine("No such option available. Please, press correct number");
						return false;
				}
			}
			return true;
		}
	}

	static class Main_Start
	{
		
		static void Main(string[] args)
		{
			Role guest = new Guest();
			string line = "";
			while (!line.Equals("exit"))
			{
				View.GuestPanel(ref line, (Guest)guest);
			}
		}
	}
}
