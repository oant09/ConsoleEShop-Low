﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
	public class Item
	{
		public string Name { get; set; }
		public int Price { get; set; }
		public string Category { get; set; }
		public string Description { get; set; }

		public Item(string name, int price, string category = "Uncategorized", string description = "No description")
		{
			Name = name;
			Price = price;
			Category = category;
			Description = description;
		}

		public string GetStringItem()
		{
			return string.Join(' ', Name, Price.ToString(), Category, Description);
		}
	}
}
