﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
	public enum OrderStatus
	{
		NewlyCreated,
		CancelledByAdministrator,
		PaymentReceived,
		Sent,
		Received,
		Completed,
		CancelledByUser
	}
}
