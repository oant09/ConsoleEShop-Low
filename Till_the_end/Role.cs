﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Shop
{
	public abstract class Role
	{
		public Role() { }

		public IEnumerable<Item> OverviewItems()
		{
			return Shop.itemList.Values.ToList();
		}

		public Item SearchItem(string itemName)
		{
			if (itemName is null)
				throw new ArgumentNullException("itemName");
			if (Shop.itemList.ContainsKey(itemName.ToLower()))
				return Shop.itemList[itemName];
			return null;
		}
	}
}
