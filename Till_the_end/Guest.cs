﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
	public class Guest : Role
	{
		public Guest() : base() { }

		public bool RegisterAccount(string userName)
		{
			if (userName is null)
				throw new ArgumentNullException("userName");
			if (Shop.CheckCanCreateUser(userName))
			{
				Shop.CreateUser(userName);
				return true;
			}
			return false;
		}

		public User LogIn(string userName)
		{
			if (userName is null)
				throw new ArgumentNullException("userName");
			return Shop.ReturnUser(userName);
		}
	}
}
