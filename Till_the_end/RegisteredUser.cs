﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
	public class RegisteredUser : User
	{
		public RegisteredUser(string userName) : base(userName) { }

		public Dictionary<int, Order> GetOrdersHistory() => Orders;

		public override string ChangeStatus(int orderNumber, OrderStatus status)
		{
			Orders[orderNumber].UpdateStatus(status);
			if (status == OrderStatus.CancelledByUser)
				return $"Order #{orderNumber} has been cancelled";
			else if (status == OrderStatus.Received)
				return $"Order #{orderNumber} has been received";
			return null;
		}

		public string ChangePersonalInfo(string newName)
		{
			if (Shop.CheckCanCreateUser(newName))
				UserName = newName;
			else
				return "User with such name already exists";
			return "Name has been changed";
		}
	}
}
