﻿using System;
using System.Collections.Generic;

namespace Shop
{
	static class PanelOptions
	{
		public static void PrintShop(IEnumerable<Item> list)
		{
			Console.Clear();
			foreach (var l in list)
			{
				Console.WriteLine("Item name\t" + l.Name);
				Console.WriteLine("  Category:\t" + l.Category);
				Console.WriteLine("  Price:\t" + l.Price);
				Console.WriteLine("  Description\t" + l.Description);
			}
		}

		public static void SearchItem(Role role)
		{
			Console.Clear();
			Console.WriteLine("Type name of item to search");
			string item = Console.ReadLine();
			if (role.SearchItem(item) != null)
				Console.WriteLine(role.SearchItem(item).GetStringItem());
			else
				Console.WriteLine("No item with such name");
		}

		public static void CreateUser(View.PrintStartPage Print)
		{
			Console.Clear();
			Console.WriteLine("Type the name of your future account");
			string input = Console.ReadLine();
			if (Shop.CheckCanCreateUser(input))
			{
				Shop.CreateUser(input);
				Console.WriteLine("Congratulations! You have created account! Log in and more options will available for you");
			}
			else
				Console.WriteLine("User with such name already exists. Choose different login");
		}

		public static bool LogIn(Guest guest)
		{
			Console.Clear();
			Console.WriteLine("Type the name of your account");
			string name = Console.ReadLine();
			User user = guest.LogIn(name);
			if (user is null)
			{
				Console.WriteLine("User with such name doesn't exist");
				return false;
			}
			if (user.GetType() == typeof(Administrator))
				View.AdminUserPanel(name, user as Administrator);
			else if (user.GetType() == typeof(RegisteredUser))
				View.RegisteredUserPanel(name, user as RegisteredUser);
			return true;
		}

		public static void CreateOrder(User user, View.PrintStartPage Print)
		{
			Console.WriteLine("Please, type name of the item you want to purchase");
			int orderNumber = user.CreateOrder(Console.ReadLine());
			if (orderNumber == -1)
				Console.WriteLine("no item with such name");
			else
				Console.WriteLine($"Order # {orderNumber} has been created");
		}

		public static void ChangeStatus(User user)
		{
			Console.WriteLine("Write your order number");
			int order;
			if (!Int32.TryParse(Console.ReadLine(), out order))
				Console.WriteLine("Order with such number doesn't exists");
			else
			{
				Console.WriteLine("If you want to confirm order press '1'. \n If you want to cancel order press '2'.");
				Int32.TryParse(Console.ReadLine(), out int input);
				switch (input)
				{
					case 1:
						Console.WriteLine(user.ChangeStatus(order, OrderStatus.Received));
						break;
					case 2:
						Console.WriteLine(user.ChangeStatus(order, OrderStatus.CancelledByUser));
						break;
					default:
						Console.WriteLine("Wrong input. Type correct option");
						break;
				}
			}
			View.PrintStartPageUser();
		}

		public static void ViewChangeUserInfo(Administrator admin)
		{
			foreach (var u in Shop.Users)
			{
				Console.WriteLine($"User:\t {u.UserName}");
			}
			Console.WriteLine("Choose user to modify");
			string input = Console.ReadLine();
			RegisteredUser user = (RegisteredUser)Shop.ReturnUser(input);
			if ( user is null)
				Console.WriteLine("No user found");
			else
			{
				Console.WriteLine(user.ChangePersonalInfo(input));
			}
			View.PrintStartPageAdmin();
		}

		public static void ChangeStatusAdmin(Administrator admin)
		{
			Console.WriteLine("Write order number");
			int order;
			if (!Int32.TryParse(Console.ReadLine(), out order))
				Console.WriteLine("Order with such number doesn't exists");
			else
			{
				Console.WriteLine("Press number to change status:");
				Console.WriteLine("1. Cancel user order. \n2.Payment received. \n3.Order sent to user\n4.Order is completed.");
				Int32.TryParse(Console.ReadLine(), out int input);
				admin.ChangeStatus(order, (OrderStatus)input);
			}
			View.PrintStartPageAdmin();
		}

		public static void ViewOrderHistory(RegisteredUser user)
		{
			foreach (var o in user.GetOrdersHistory())
			{
				Console.WriteLine($"Order # {o.Key} \tStatus: {o.Value.Status}");
			}
			if (user.GetOrdersHistory().Count == 0)
				Console.WriteLine("Your order history is empty yet");
			View.PrintStartPageUser();
		}

		public static void ChangeInfo(RegisteredUser user)
		{
			Console.WriteLine("Type the name of your account");
			string input = Console.ReadLine();
			Console.WriteLine(user.ChangePersonalInfo(input));
			Console.Clear();
			View.PrintStartPageUser();
		}
		public static void AddNewItem(Administrator admin)
		{
			Console.WriteLine("Type item name");
			string name = Console.ReadLine();
			Console.WriteLine("Type item category");
			string category = Console.ReadLine();
			Console.WriteLine("Type price of item");
			Int32.TryParse(Console.ReadLine(), out int price);
			Console.WriteLine("Type item description");
			string descr = Console.ReadLine();
			admin.AddItem(name, category, price, descr);
			Console.WriteLine("New item added");
			View.PrintStartPageAdmin();
		}

		public static void ChangeItemInfo(Administrator admin)
		{
			Console.WriteLine("Type name of item to search");
			string item = Console.ReadLine();
			if (admin.SearchItem(item) != null)
				Console.WriteLine(admin.SearchItem(item).GetStringItem());
			else
				Console.WriteLine("No item with such name");
			admin.SearchItem(item);
			Console.WriteLine("Type item name");
			string name = Console.ReadLine();
			Console.WriteLine("Type item category");
			string category = Console.ReadLine();
			Console.WriteLine("Type price of item");
			Int32.TryParse(Console.ReadLine(), out int price);
			Console.WriteLine("Type item description");
			string descr = Console.ReadLine();
			admin.ChangeItemInfo(item, name, category, price, descr);
			Console.WriteLine("Item info updated");
			View.PrintStartPageAdmin();
		}
	}
}
