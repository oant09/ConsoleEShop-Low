﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
	public class Order
	{
		public int OrderNumber { get; private set; }
		public OrderStatus Status { get; set; }
		public Item itemOrdered { get; private set; }
		public User CreatedBy { get; private set; }

		public Order(Item item, User user, int orderNumber)
		{
			OrderNumber = orderNumber;
			itemOrdered = item;
			CreatedBy = user;
			Status = OrderStatus.NewlyCreated;
		}

		public void UpdateStatus(OrderStatus newStatus)
		{
			Status = newStatus;
		}
	}
}
