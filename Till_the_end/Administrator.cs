﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
	public class Administrator : User
	{
		public Administrator(string userName) : base(userName) { }

		public override string ChangeStatus(int orderNumber, OrderStatus status)
		{
			Shop.Orders[orderNumber].UpdateStatus(status);

			switch (status)
			{
				case OrderStatus.CancelledByAdministrator:
					return $"Order #{orderNumber} has been cancelled by administrator";
				case OrderStatus.PaymentReceived:
					return $"Order #{orderNumber} payment received";
				case OrderStatus.Sent:
					return $"Order #{orderNumber} sent to user";
				case OrderStatus.Completed:
					return $"Order #{orderNumber} completed";
				default:
					return null;
			}
		}

		public IEnumerable<User> ViewAllUsers()
		{
			return Shop.Users;
		}

		public void ChangeUserInfo(string oldUserName, string newUserName)
		{
			Shop.ReturnUser(oldUserName).UserName = newUserName;
		}

		public void AddItem(string itemName, string category, int price, string description)
		{
			Item item = new Item(itemName, price, category, description);
			Shop.itemList.Add(item.Name, item);
		}

		public void ChangeItemInfo(string itemName, string newName, string category, int price, string description)
		{
			if (category != null)
				Shop.itemList[itemName].Category = category;
			if (price > 0)
				Shop.itemList[itemName].Price = price;
			if (description != null)
				Shop.itemList[itemName].Description = description;
			if (newName != null)
				Shop.itemList[itemName].Name = newName;
		}
	}
}
