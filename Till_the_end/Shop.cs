﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Shop
{
	public static class Shop
	{
		public static int CountOrders { get => Orders.Count; }
		public static List<User> Users { get; set; }
		public static Dictionary<int, Order> Orders { get; set; }
		public static Dictionary<string, Item> itemList { get; set; }

		static Shop()
		{
			Orders = new Dictionary<int, Order>();
			Users = new List<User>();
			itemList = new Dictionary<string, Item>();
			Users.Add(new Administrator("administrator"));
			itemList.Add("monitor", new Item("monitor", 300, "Computers"));
			itemList.Add("keyboard", new Item("keyboard", 100, "Computers"));
			itemList.Add("laptop", new Item("laptop", 1000, "Computers"));
			itemList.Add("smart watch", new Item("smart watch", 400, "Computers"));
		}

		public static Order CreateOrder(Item itemOrdered, User user)
		{
			Order newOrder = new Order(itemOrdered, user, CountOrders + 1);
			Orders.Add(newOrder.OrderNumber, newOrder);
			return newOrder;
		}

		public static bool CheckCanCreateUser(string userName)
		{
			return Users.Where(w => w.UserName == userName).Count() == 0 ? true : false ;
		}

		public static void CreateUser(string userName)
		{
			Users.Add(new RegisteredUser(userName)); 
		}

		public static User ReturnUser(string userName)
		{
			return Users.Find(w => w.UserName.Equals(userName));
		}
	}
}
