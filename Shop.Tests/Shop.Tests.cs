using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Shop;


namespace Shop.Tests
{
	public class Tests
	{
		[SetUp]
		public void Setup()
		{
		}

		Guest guest = new Guest();
		RegisteredUser user = new RegisteredUser("name");
		Administrator admin = new Administrator("Administrator");

		[Test]
		public void CreateAccount_WithNullInput()
		{
			string name = null;
			var expectedEx = typeof(ArgumentNullException);
			var actEx = Assert.Catch(() => guest.RegisterAccount(name));
			Assert.AreEqual(expectedEx, actEx.GetType(), message: "userName");
		}

		[Test]
		public void CheckLogInCorrectness()
		{
			guest.RegisterAccount("DartVeider");
			User user = guest.LogIn("DartVeider");
			Assert.AreEqual(user.UserName, "DartVeider");
		}

		[TestCase("monitor")]
		[TestCase("laptop")]
		[TestCase("keyboard")]
		public void CheckSearchItem(string item)
		{
			string itemName = "speakers";
			Item shopItem;
			Shop.itemList.TryGetValue(itemName, out shopItem);
			Item testItem = guest.SearchItem("speakers");
			Assert.AreEqual(shopItem, testItem);
		}

		[Test]
		public void CheckOverviewShop()
		{
			IEnumerable<Item>  itemList = Shop.itemList.Values;
			IEnumerable<Item> itemListTest = guest.OverviewItems();
			Assert.AreEqual(itemList, itemListTest);
		}

		[Test]
		public void TestSearchItemDoesntExist()
		{
			Item item = null;
			Item searchItem = guest.SearchItem("qwerty");
			Assert.AreEqual(item, searchItem);
		}

		[Test]
		public void TestCreateOrder()
		{
			Item orderItem = user.SearchItem("monitor");
			Order userOrder = Shop.CreateOrder(orderItem, user);
			user.Orders.Add(userOrder.OrderNumber, userOrder);
			Order shopOrder = Shop.Orders.GetValueOrDefault(userOrder.OrderNumber);
			Assert.AreEqual(userOrder, shopOrder);
		}

		[Test]
		public void TestConfirmOrder()
		{
			Item orderItem = user.SearchItem("monitor");
			int userOrderNumber = user.CreateOrder("monitor");
			user.ConfirmOrder(userOrderNumber);
			Order userOrder = user.Orders[userOrderNumber];
			Assert.AreEqual(userOrder.Status, OrderStatus.Received);
		}

		[TestCase (OrderStatus.CancelledByAdministrator)]
		[TestCase (OrderStatus.PaymentReceived)]
		[TestCase (OrderStatus.Sent)]
		public void TestChangeStatus(OrderStatus status)
		{
			Item orderItem = user.SearchItem("monitor");
			int userOrderNumber = user.CreateOrder("monitor");
			Order userOrder = user.Orders[userOrderNumber];
			admin.ChangeStatus(userOrderNumber, status);
			Assert.AreEqual(userOrder.Status, status);
		}

		[Test]
		public void TestChangeUserInfo()
		{
			string oldName = "oldName";
			string newName = "newName";
			Shop.CreateUser(oldName);
			RegisteredUser user2 = Shop.ReturnUser(oldName) as RegisteredUser;
			RegisteredUser userChanged = user2;
			admin.ChangeUserInfo(oldName, newName);
			Assert.AreEqual(newName, user2.UserName);
			Assert.AreSame(user2, userChanged);
		}

		[Test]
		public void TestAddItem()
		{
			Item item = new Item("case", 10, null, null);
			admin.AddItem("case", null, 10, null);
			Item itemAdded;
			Shop.itemList.TryGetValue("case", out itemAdded);
			Assert.AreEqual(item.Name, itemAdded.Name);
		}

		[Test]
		public void TestChangeItemInfo()
		{
			admin.AddItem("powerbank", null, 10, null);
			Item itemAdded;
			Shop.itemList.TryGetValue("powerbank", out itemAdded);
			admin.ChangeItemInfo("powerbank", "charger", null, 22, null);
			Item itemChanged;
			Shop.itemList.TryGetValue("powerbank", out itemChanged);
			Assert.AreEqual(itemChanged.Name, itemAdded.Name);
			Assert.AreSame(itemChanged, itemAdded);
		}
	}
}